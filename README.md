# Brute_subdomain_golang

### 介绍
使用golang语言所写的域名爆破工具，可以使用Golang语言直接编译

编译方式

go build

运行方式

del log.txt && del port.txt && main.exe -u baidu.com -f dic.txt -t 200 -p 21,22,23,8090,3389,6379

#### 功能
* 0x01

* 提供了高效的子域名爆破效率

* 0x02

* 提供了对爆破出子域IP端口的扫描

* 0x03

* 提供了强大的字典
